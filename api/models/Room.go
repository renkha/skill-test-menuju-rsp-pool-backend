package models

import (
	"errors"
	"html"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
)

// Room is
type Room struct {
	ID           uint32    `gorm:"primary_key;auto_increment" json:"id"`
	RoomName     string    `gorm:"size:255;not null;unique" json:"room_name"`
	RoomCapacity uint32    `gorm:"not null;" json:"room_capacity"`
	Photo        string    `gorm:"size:100;not null;" json:"photo"`
	CreatedAt    time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt    time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
	DeletedAt    time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"deleted_at"`
}

// Prepare is
func (r *Room) Prepare() {
	r.ID = 0
	r.RoomName = html.EscapeString(strings.TrimSpace(r.RoomName))
	r.RoomCapacity = 0
	r.CreatedAt = time.Now()
	r.UpdatedAt = time.Now()
}

// Validate is
func (r *Room) Validate(action string) error {

	if r.RoomName == "" {
		return errors.New("Required RoomName")
	}
	if r.RoomCapacity == 0 {
		return errors.New("Required RoomCapacity")
	}
	if r.Photo == "" {
		return errors.New("Required Photo")
	}
	return nil
}

// SaveRoom is
func (r *Room) SaveRoom(db *gorm.DB) (*Room, error) {

	var err error
	err = db.Debug().Create(&r).Error
	if err != nil {
		return &Room{}, err
	}
	return r, nil
}

// FindAllRooms is
func (r *Room) FindAllRooms(db *gorm.DB) (*[]Room, error) {
	var err error
	rooms := []Room{}
	err = db.Debug().Model(&Room{}).Limit(100).Find(&rooms).Error
	if err != nil {
		return &[]Room{}, err
	}
	return &rooms, err
}

// FindRoomByID is
func (r *Room) FindRoomByID(db *gorm.DB, uid uint32) (*Room, error) {
	var err error
	err = db.Debug().Model(Room{}).Where("id = ?", uid).Take(&r).Error
	if err != nil {
		return &Room{}, err
	}
	if gorm.IsRecordNotFoundError(err) {
		return &Room{}, errors.New("Room Not Found")
	}
	return r, err
}

// UpdateARoom is
func (r *Room) UpdateARoom(db *gorm.DB, uid uint32) (*Room, error) {

	var err error
	err = db.Debug().Model(&Room{}).Where("id = ?", r.ID).Updates(Room{
		RoomName:     r.RoomName,
		RoomCapacity: r.RoomCapacity,
		Photo:        r.Photo,
		UpdatedAt:    time.Now(),
	}).Error

	if err != nil {
		return &Room{}, err
	}

	return r, nil
}

// DeleteARoom is
func (r *Room) DeleteARoom(db *gorm.DB, uid uint32) (int64, error) {

	db = db.Debug().Model(&Room{}).Where("id = ?", uid).Take(&Room{}).Delete(&Room{})

	if db.Error != nil {
		return 0, db.Error
	}
	return db.RowsAffected, nil
}
