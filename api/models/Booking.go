package models

import (
	"errors"
	"html"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
)

//Booking is ...
type Booking struct {
	ID           uint64    `gorm:"primary_key;auto_increment" json:"id"`
	CustomerID   uint32    `sql:"type:int REFERENCES users(id)" json:"user_id"`
	Customer     User      `json:"customer"`
	RoomID       uint32    `sql:"type:int REFERENCES rooms(id)" json:"room_id"`
	RoomBooked   Room      `json:"room_booked"`
	TotalPerson  uint32    `gorm:"size:255;not null;" json:"total_person"`
	Noted        string    `gorm:"size:255;not null;" json:"noted"`
	BookingTime  time.Time `gorm:"type:datetime" json:"booking_time"`
	CheckInTime  time.Time `gorm:"type:datetime" json:"check_in_time"`
	CheckOutTime time.Time `gorm:"type:datetime" json:"check_out_time"`
	CreatedAt    time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt    time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

//Prepare is
func (b *Booking) Prepare() {
	b.ID = 0
	b.Customer = User{}
	b.RoomBooked = Room{}
	b.TotalPerson = 0
	b.Noted = html.EscapeString(strings.TrimSpace(b.Noted))
	b.BookingTime = time.Now()
	b.CheckInTime = time.Now()
	b.CheckOutTime = time.Now()
	b.CreatedAt = time.Now()
	b.UpdatedAt = time.Now()
}

//Validate is
func (b *Booking) Validate() error {

	if b.Customer == (User{}) {
		return errors.New("Required Customer")
	}
	if b.RoomBooked == (Room{}) {
		return errors.New("Required Room for Booked")
	}
	if b.Noted == "" {
		return errors.New("Required Noted")
	}
	if b.BookingTime == (time.Time{}) {
		return errors.New("Required Booking Time")
	}
	return nil
}

//SaveBooking is
func (b *Booking) SaveBooking(db *gorm.DB) (*Booking, error) {
	var err error
	err = db.Debug().Model(&Booking{}).Create(&b).Error
	if err != nil {
		return &Booking{}, err
	}
	if b.ID != 0 {
		err = db.Debug().Model(&User{}).Where("id = ?", b.CustomerID).Take(&b.Customer).Error
		if err != nil {
			return &Booking{}, err
		}
	}
	return b, nil
}

// FindAllBookings is
func (b *Booking) FindAllBookings(db *gorm.DB) (*[]Booking, error) {
	var err error
	bookings := []Booking{}
	err = db.Debug().Model(&Booking{}).Limit(100).Find(&bookings).Error
	if err != nil {
		return &[]Booking{}, err
	}
	if len(bookings) > 0 {
		for i := range bookings {
			err := db.Debug().Model(&User{}).Where("id = ?", bookings[i].CustomerID).Take(&bookings[i].Customer).Error
			if err != nil {
				return &[]Booking{}, err
			}
		}
	}
	return &bookings, nil
}

// FindBookingByID is
func (b *Booking) FindBookingByID(db *gorm.DB, pid uint64) (*Booking, error) {
	var err error
	err = db.Debug().Model(&Booking{}).Where("id = ?", pid).Take(&b).Error
	if err != nil {
		return &Booking{}, err
	}
	if b.ID != 0 {
		err = db.Debug().Model(&User{}).Where("id = ?", b.CustomerID).Take(&b.Customer).Error
		if err != nil {
			return &Booking{}, err
		}
	}
	return b, nil
}

// UpdateABooking is
func (b *Booking) UpdateABooking(db *gorm.DB) (*Booking, error) {

	var err error
	err = db.Debug().Model(&Booking{}).Where("id = ?", b.ID).Updates(Booking{
		TotalPerson: b.TotalPerson,
		Noted:       b.Noted,
		BookingTime: b.BookingTime,
		UpdatedAt:   time.Now(),
	}).Error
	if err != nil {
		return &Booking{}, err
	}
	if b.ID != 0 {
		err = db.Debug().Model(&User{}).Where("id = ?", b.CustomerID).Take(&b.Customer).Error
		if err != nil {
			return &Booking{}, err
		}
	}
	return b, nil
}

// UpdateCheckInAndOutTime is
func (b *Booking) UpdateCheckInAndOutTime(db *gorm.DB) (*Booking, error) {

	var err error

	if checkTime := db.Debug().Model(&Booking{}).Where("id = ?", b.ID).Where("check_in_time = ?", ""); checkTime != nil {
		err = db.Debug().Model(&Booking{}).Where("id = ?", b.ID).Updates(Booking{
			CheckInTime: time.Now(),
			UpdatedAt:   time.Now(),
		}).Error
	} else {
		err = db.Debug().Model(&Booking{}).Where("id = ?", b.ID).Updates(Booking{
			CheckOutTime: time.Now(),
			UpdatedAt:    time.Now(),
		}).Error
	}
	if err != nil {
		return &Booking{}, err
	}
	if b.ID != 0 {
		err = db.Debug().Model(&User{}).Where("id = ?", b.CustomerID).Take(&b.Customer).Error
		if err != nil {
			return &Booking{}, err
		}
	}
	return b, nil
}

// DeleteABooking is
func (b *Booking) DeleteABooking(db *gorm.DB, bid uint64, uid uint32) (int64, error) {

	db = db.Debug().Model(&Booking{}).Where("id = ? and customer_id = ?", bid, uid).Take(&Booking{}).Delete(&Booking{})

	if db.Error != nil {
		if gorm.IsRecordNotFoundError(db.Error) {
			return 0, errors.New("Booking not found")
		}
		return 0, db.Error
	}
	return db.RowsAffected, nil
}
