package controllers

import "api-rsp-skilltest/api/middlewares"

func (s *Server) initializeRoutes() {

	// Home Route
	s.Router.HandleFunc("/", middlewares.SetMiddlewareJSON(s.Home)).Methods("GET")

	// Login Route
	s.Router.HandleFunc("/login", middlewares.SetMiddlewareJSON(s.Login)).Methods("POST")

	//Users routes
	s.Router.HandleFunc("/users", middlewares.SetMiddlewareJSON(s.CreateUser)).Methods("POST")
	s.Router.HandleFunc("/users", middlewares.SetMiddlewareJSON(s.GetUsers)).Methods("GET")
	s.Router.HandleFunc("/users/{id}", middlewares.SetMiddlewareJSON(s.GetUser)).Methods("GET")
	s.Router.HandleFunc("/users/{id}", middlewares.SetMiddlewareJSON(middlewares.SetMiddlewareAuthentication(s.UpdateUser))).Methods("PUT")
	s.Router.HandleFunc("/users/{id}", middlewares.SetMiddlewareAuthentication(s.DeleteUser)).Methods("DELETE")

	//Rooms routes
	s.Router.HandleFunc("room", middlewares.SetMiddlewareJSON(s.CreateRoom)).Methods("POST")
	s.Router.HandleFunc("/rooms", middlewares.SetMiddlewareJSON(s.GetRooms)).Methods("GET")
	s.Router.HandleFunc("/rooms/{id}", middlewares.SetMiddlewareJSON(s.GetRoom)).Methods("GET")
	s.Router.HandleFunc("/rooms/{id}", middlewares.SetMiddlewareJSON(middlewares.SetMiddlewareAuthentication(s.UpdateRoom))).Methods("PUT")
	s.Router.HandleFunc("/rooms/{id}", middlewares.SetMiddlewareAuthentication(s.DeleteRoom)).Methods("DELETE")

	//Bookings routes
	s.Router.HandleFunc("/bookings", middlewares.SetMiddlewareJSON(s.CreateBooking)).Methods("POST")
	s.Router.HandleFunc("/bookings", middlewares.SetMiddlewareJSON(s.GetBookings)).Methods("GET")
	s.Router.HandleFunc("/bookings/{id}", middlewares.SetMiddlewareJSON(s.GetBooking)).Methods("GET")
	s.Router.HandleFunc("/bookings/{id}", middlewares.SetMiddlewareJSON(middlewares.SetMiddlewareAuthentication(s.UpdateBooking))).Methods("PUT")
	s.Router.HandleFunc("/bookings/{id}", middlewares.SetMiddlewareAuthentication(s.DeleteBooking)).Methods("DELETE")
}
