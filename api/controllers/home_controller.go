package controllers

import (
	"net/http"

	"api-rsp-skilltest/api/responses"
)

// Home is
func (server *Server) Home(w http.ResponseWriter, r *http.Request) {
	responses.JSON(w, http.StatusOK, "Welcome To API - RSP Skill Test")

}
