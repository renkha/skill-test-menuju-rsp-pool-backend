package seed

// import (
// 	"log"
// 	"time"

// 	"api-rsp-skilltest/api/models"

// 	"github.com/jinzhu/gorm"
// )

// var users = []models.User{
// 	{
// 		Email:    "steven@gmail.com",
// 		Password: "password",
// 		Photo:    "asdasd",
// 	},
// 	{
// 		Email:    "luther@gmail.com",
// 		Password: "password",
// 		Photo:    "asdasd",
// 	},
// }

// var rooms = []models.Room{
// 	{
// 		RoomName:     "Qwe A",
// 		RoomCapacity: 5,
// 		Photo:        "asdasd",
// 	},
// 	{
// 		RoomName:     "Qwe B",
// 		RoomCapacity: 5,
// 		Photo:        "asdasd",
// 	},
// }

// var bookings = []models.Booking{
// 	{
// 		CustomerID:   1,
// 		Customer:     models.User{},
// 		RoomID:       1,
// 		RoomBooked:   models.Room{},
// 		TotalPerson:  1,
// 		Noted:        "asd",
// 		BookingTime:  time.Now(),
// 		CheckInTime:  time.Now(),
// 		CheckOutTime: time.Now(),
// 	},
// 	{
// 		CustomerID:   2,
// 		Customer:     models.User{},
// 		RoomID:       2,
// 		RoomBooked:   models.Room{},
// 		TotalPerson:  2,
// 		Noted:        "asdasd",
// 		BookingTime:  time.Now(),
// 		CheckInTime:  time.Now(),
// 		CheckOutTime: time.Now(),
// 	},
// }

// // Load is
// func Load(db *gorm.DB) {

// 	err := db.Debug().DropTableIfExists(&models.Booking{}, &models.User{}, &models.Room{}).Error
// 	if err != nil {
// 		log.Fatalf("cannot drop table: %v", err)
// 	}
// 	err = db.Debug().AutoMigrate(&models.User{}, &models.Room{}, &models.Booking{}).Error
// 	if err != nil {
// 		log.Fatalf("cannot migrate table: %v", err)
// 	}

// 	for i := range users {
// 		err = db.Debug().Model(&models.User{}).Create(&users[i]).Error
// 		if err != nil {
// 			log.Fatalf("cannot seed users table: %v", err)
// 		}
// 		bookings[i].CustomerID = users[i].ID

// 		err = db.Debug().Model(&models.Booking{}).Create(&bookings[i]).Error
// 		if err != nil {
// 			log.Fatalf("cannot seed bookings table: %v", err)
// 		}
// 	}

// 	for i := range rooms {
// 		err = db.Debug().Model(&models.Room{}).Create(&rooms[i]).Error
// 		if err != nil {
// 			log.Fatalf("cannot seed rooms table: %v", err)
// 		}
// 		bookings[i].RoomID = rooms[i].ID

// 		err = db.Debug().Model(&models.Booking{}).Create(&bookings[i]).Error
// 		if err != nil {
// 			log.Fatalf("cannot seed bookings table: %v", err)
// 		}
// 	}
// }
